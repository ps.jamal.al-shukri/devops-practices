FROM openjdk:8
MAINTAINER JamalAlshukri
ENV port="8090"
ENV database_profile="mysql"
ENV database_url="jdbc:mysql://database:3306/assignment?allowPublicKeyRetrieval=true"
ENV database_username="assignment"
ENV database_password="assignment"
EXPOSE 8090
COPY ./target/assignment-*.jar /usr/local/
RUN chmod +x /usr/local/assignment-*.jar
ENTRYPOINT java -jar -Dserver.port=$port -Dspring.profiles.active=$database_profile -Dspring.datasource.username=$database_username -Dspring.datasource.password=$database_password -Dspring.datasource.url=$database_url /usr/local/assignment-*.jar